# [Écosystème](https://defelice.up8.site/poofichiers/projetEco.pdf)

<details><summary>Informations UP8</summary>

|                     |                                 |
|--------------------:|---------------------------------|
|                 Nom | Anri KENNEL                     |
|              Classe | L2-X                            |
|   Numéro d'étudiant | 20010664                        |
|                Mail | anri.kennel@etud.univ-paris8.fr |
| Cycle universitaire | 2021-2022                       |
|              Module | Programmation orientée objet    |
</details>

## But
Programme qui simule un univers cohérent composé de quelques animaux et végétaux.

## Description
![](presentation/img/schema.svg)
- Animaux placés aléatoirement dans l'univers au début de la simulation
- Déplacement des animaux d'une case par tour (soit 9 possibilités)
- Interface texte avec un repère style echequier
- Reproduction des espèces

## Utilisation
### Récupération et compilation
Cloner le programme avec soit :
- En SSH : `git clone git@code.up8.edu:Anri/ecosyteme.git`
- En HTTPS : `git clone https://git.kennel.ml/Paris8/Ecosyteme.git`
Une fois dans le projet, pour compiler le programme, utilisez `make`.
> Pour compiler le programme avec les options de debug, il est possible de faire `make dev`.

### Utilisation
- `./ecosyteme` lance le programme avec les paramètres par défaut, soit :
    - Un univers 5x6
    - 9 moutons
    - 4 loups
- `./ecosysteme m n M L` avec :
    - `m` la longueur et `n` la largeur de l'univers
    - `M` le nombre de moutons
    - `L` le nombre de loups
<!-- - `./ecosyteme -b m n [e]` (ou `./ecosyteme --benchmark m n [e]`) avec :
    - `-b` ou `--benchmark` permet de lancer plusieurs univers différents en même temps et ainsi calculer les meilleurs paramètres pour une taille d'univers donné
    - `e` est un argument faculatatif et permet de forcer le nombre d'échantillon que le programme va lancer pour déterminer les meilleurs paramètres. -->

## Améliorations
- [ ] Possibilité de sauvegarder/charger un univers existant
- [ ] Possibilité de mettre la simulation en pause (et de la reprendre)
- [ ] Déplacement des animaux intelligent
- [x] Rythme de reproduction (louve ne peut faire un petit qu’une fois tous les n tours)
- [ ] Trouve les meilleurs paramètres pour faire durer l’univers le plus longtemps possible
- [ ] Lancer plusieurs simulation en même temps

---
### Infos
- Testé avec `g++ (GCC) 11.2.0`.
- Débuggé avec `valgrind-3.17.0` et `valgrind-3.18.1` (arguments utilisés : `--leak-check=full --show-leak-kinds=all --track-origins=yes -s`).
- J'ai essayé de suivre les [bonne pratiques détaillés dans ce document](https://github.com/cpp-best-practices/cppbestpractices/blob/master/00-Table_of_Contents.md).
