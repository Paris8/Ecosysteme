#ifndef ECOSYSTEME_ORGANISME_HPP
#define ECOSYSTEME_ORGANISME_HPP 1

#include <map>
#include <system_error>
#include <iostream>

class Organisme {
    inline static int m_total_ID; // permet d'incrémenter de 1 l'ID de chaque organisme

    protected:
        const int m_univers_ID; // stocke l'ID de l'univers où l'organisme est présent

        // Stocke la table des correspondances,
        // chaque organisme possède un ID unique et est
        // représenté par une lettre, cette association est
        // réalisé dans cette map
        static inline std::map<int, char> m_correspondance;

        int m_index; // Location dans l'univers

        // Supprime l'organisme des vecteur
        void suppresionVecteurs(void) noexcept;

    public:
        const int ID; // ID unique pour chaque organisme

        const bool m_superposable; // Type de l'organisme

        bool mort = false; // vrai si l'organisme est mort

        // ID de l'Univers, type de l'organisme, index dans l'univers
        Organisme(int, bool = true, int = -1);

        virtual ~Organisme(void);

        // Renvoie la lettre correspondant à l'ID
        static char lettre(int) noexcept;

        // Définit le comportement de l'organisme à chaque tour
        virtual void action(void) = 0;

        // Renvoie les positions exact de l'organisme dans l'univers donnée (index/x;y)
        std::pair<int, std::pair<int, int>> position(void) const noexcept;

        // Renvoie les coordonées de l'organisme en format "echequier"
        std::string coordoneeeEchequier(void) const noexcept;

        // Fais mourir l'organisme en libérant sa position
        void mortOrganisme(void) noexcept;
};

#endif
