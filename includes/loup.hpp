#ifndef ECOSYSTEME_LOUP_HPP
#define ECOSYSTEME_LOUP_HPP 1

#include "animal.hpp"
#include "mouton.hpp"

class Loup: public Animal {
    const char _m_lettre = 'L'; // caractère représentant le loup

    // Définie la vitesse du loup
    int generationVitesse(void) const noexcept;

    public:
        // ID de l'univers
        Loup(int);

        // ID de l'univers, index dans l'univers
        Loup(int, int);

        // Renvoie vrai
        bool carnivore(void) const noexcept;

        // Définit le comportement du loup à chaque tour
        void action(void);
};

#endif
