#ifndef ECOSYSTEME_SEL_HPP
#define ECOSYSTEME_SEL_HPP 1

#include "herbe.hpp"

class Sel: public Organisme {
    const char _m_lettre = '-'; // caractère représentant le sel

    int m_age = 0;

    public:
        // ID de l'univers, index dans l'univers
        Sel(int, int);

        // ID de l'univers
        Sel(int);

        // Remplace le sel par de l'herbe
        ~Sel(void);

        // Définit le comportement du sel à chaque tour
        void action(void);
};

#endif
