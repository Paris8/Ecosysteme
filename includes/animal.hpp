#ifndef ECOSYSTEME_ANIMAL_HPP
#define ECOSYSTEME_ANIMAL_HPP 1

#include "univers.hpp"
#include "sel.hpp"

/* Un animal possède une vitesse, plus il est rapide,
 * plus vite il se déplacera à chaque tour, par exemple :
 * dans le cas où un chasseur et un chassé s'ont l'un à côté
 * de l'autre, si le chasseur se déplace plus vite, il mangera
 * le chassé, en revanche, si c'est le chassé qui a une meilleure
 * vitesse, alors c'est lui qui se déplacera en premier et il
 * fuira */

class Animal: public Organisme {
    template<typename Espece>
    void rechercheEspece(int, std::vector<Espece *> &) noexcept;

    template<typename Espece>
    void animauxEnvirons(std::vector<Espece *> &) noexcept;

    protected:
        const int m_age_max; // age maximale que peut atteindre l'organisme

        const int m_faim_max; // faim maximal que peut atteindre l'animal avant de mourir

        int m_faim = 0; // faim de l'animal

        int m_age = 0; // age actuel de l'organisme

        // nombre de tour avant accouchement (-1 = quand pas enceinte)
        short m_reproduire = -1;

        Animal * m_partenaire = nullptr;

        const int genre; // genre, 0 = masculin, 1 = féminin

        // Nombre de tour a attendre avant de pouvoir se reproduire de nouveau
        const short m_attente_reproduction;

        // Définit les repas que doit prendre l'animal, par exemple
        // 1 = 1 repas par tour
        // 3 = 1 repas tout les 3 tours
        const short m_repas_tt_les_cb_de_tours;

        // Renvoie une vitesse aléatoire (+ élevé = + rapide)
        virtual int generationVitesse(void) const noexcept = 0;

        // Renvoie un genre choisit aléatoirement
        int choixGenre(void) const noexcept;

        // Renvoie la liste des cases atours de l'animal
        std::vector<int> casesAlentours(void) const noexcept;

        // Renvoie la liste des cases accesible depuis la position de l'animal
        std::vector<int> casesPossible(void) const noexcept;

        template<typename Espece>
        void procreation(void) noexcept;

        // Déplace l'animal
        void deplacement(void) noexcept;
        void deplacement(int, bool = true) noexcept;

    public:
        // Vrai si l'animal doit déposé des sédiment au sol à sa mort
        bool m_deposer_sediment = true;

        const int vitesse; // vitesse de l'organisme

        // ID de l'univers, age max, faim max, vitesse, genre, rythme reproduction, repas tout les n de tours
        Animal(int, int, int, int, int, int, int);

        // ID de l'univers, index dans l'univers, age max, faim max, vitesse, genre, rythme reproduction, repas tout les n de tours
        Animal(int, int, int, int, int, int, int, int);

        ~Animal(void);

        // Animal carnivore ?
        virtual bool carnivore(void) const noexcept = 0;
};

#include "animal_template.hpp" // définition du template

#endif
