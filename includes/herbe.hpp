#ifndef ECOSYSTEME_HERBE_HPP
#define ECOSYSTEME_HERBE_HPP 1

#include "organisme.hpp"

class Herbe: public Organisme {
    const char _m_lettre = ' '; // caractère représentant l'herbe

    public:
        // ID de l'univers, index dans l'univers
        Herbe(int, int);

        // ID de l'univers
        Herbe(int);

        ~Herbe(void);

        // Définit le comportement de l'herbe à chaque tour
        void action(void);
};

#endif
