#ifndef ECOSYSTEME_UNIVERS_HPP
#define ECOSYSTEME_UNIVERS_HPP 1

#include <algorithm>
#include <random>

#include "herbe.hpp"

class Univers {
    friend class Organisme;
    friend class Evenement;

    const int m_longueur, m_largeur, m_taille_univers; // dimensions de l'univers
    int m_tour; // âge de l'univers

    inline static int m_total_ID; // s'incrémente à chaque création d'univers

    // Mélange le contenu d'un vecteur
    static void melange(std::vector<int> * vecteur) noexcept;

    public:
        // Stocke pour chaque ID d'univers les dimensions de l'univers (largeur, longueur)
        inline static std::map<int, std::pair<int, int>> m_dimensions_univers;

        // Stocke pour chaque ID d'univers les organismes présent dans l'univers
        inline static std::map<int, std::vector<Organisme *>> m_organismes_univers;

        // Stocke pour chaque ID d'univers les index libres
        // first  => organismes qui peuvent se superposer (herbe, sel...)
        // second => organismes qui ne le peuvent pas (animaux)
        inline static std::map<int, std::pair<std::vector<int>, std::vector<int>>> m_index_libres_univers;

        const int ID; // ID unique pour chaque univers

        // longueur, largeur
        Univers(int, int);

        ~Univers(void);

        // Vérifie s'il y a de la vie dans l'univers
        bool enVie(void) const noexcept;

        // Supprime les organismes mort du vecteur qui les stockes
        void nettoyageMorts(void) noexcept;
};

#endif
