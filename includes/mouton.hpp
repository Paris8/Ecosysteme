#ifndef ECOSYSTEME_MOUTON_HPP
#define ECOSYSTEME_MOUTON_HPP 1

#include "animal.hpp"

class Mouton: public Animal {
    const char _m_lettre = 'M'; // caractère représentant le mouton

    // Défini la vitesse du mouton
    int generationVitesse(void) const noexcept;

    public:
        // ID de l'univers
        Mouton(int);

        // ID de l'univers, index dans l'univers
        Mouton(int, int);

        // Renvoie faux
        bool carnivore(void) const noexcept;

        // Définit le comportement du mouton à chaque tour
        void action(void);
};

#endif
