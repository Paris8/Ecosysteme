#ifndef ECOSYSTEME_EVENEMENT_HPP
#define ECOSYSTEME_EVENEMENT_HPP 1

#include <iostream>

#include "mouton.hpp"
#include "loup.hpp"

struct Evenement {
    Evenement() = delete;

    // Créer un univers pour préparer la simulation
    static void creationSimulation(Univers **, int, int, int, int);

    // Lance la simulation pour un univers
    static void lancerSimulation(Univers *);

    // Arrête la simulation d'un univers
    static void arreterSimulation(Univers *);

    // Affiche un univers, possibilité d'afficher les
    // traits séparant les valeurs du tableaux (par défaut : oui)
    static void affichage(Univers *, bool = true) noexcept;

    private:
        struct comp_organisme {
            bool operator()(Organisme * organisme_a, Organisme * organisme_b) const {
                int vitesse_a, vitesse_b;

                // Si l'organisme est un animal, on utilises sa vitesse pour
                // la comparaison, sinon on dit que la vitesse est "infini"
                // ce qui permet de placer l'organisme au début du vecteur

                if(Animal * animal_a = dynamic_cast<Animal *>(organisme_a)) {
                    vitesse_a = animal_a->vitesse;
                } else {
                    vitesse_a = (int)INFINITY;
                }
                if(Animal * animal_b = dynamic_cast<Animal *>(organisme_b)) {
                    vitesse_b = animal_b->vitesse;
                } else {
                    vitesse_b = (int)INFINITY;
                }

                // Si les vitesses sont égales, alors c'est l'ID
                // (un peu comme la date de création / age) de l'organisme
                // qui décide car + l'ID est faible, + tôt l'organisme à été
                // créer, et on part du principe que + on est jeune + on est rapide
                // (on n'utilises pas l'âge car un organisme n'a pas d'âge)
                if(vitesse_a == vitesse_b) { // cas rare
                    return organisme_a->ID > organisme_b->ID;
                }

                return vitesse_a > vitesse_b;
            }
        };
};

#endif
