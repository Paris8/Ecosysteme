#include "../includes/sel.hpp"

Sel::Sel(const int univers_ID, const int index): Organisme(univers_ID, true, index) {
    m_correspondance[ID] = _m_lettre;
}

Sel::Sel(const int univers_ID): Organisme(univers_ID) {
    m_correspondance[ID] = _m_lettre;
}

Sel::~Sel(void) {
    suppresionVecteurs();

    // On ajoute une herbe à la place
    new Herbe(m_univers_ID, m_index);
}

void Sel::action(void) {
    if(m_age == 1) { // devient de l'herbe au bout d'un tour
        mortOrganisme();
    } else {
        ++m_age;
    }
}
