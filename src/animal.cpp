#include "../includes/animal.hpp"

Animal::Animal(const int univers_ID, const int index, const int age_max,
               const int faim_max, const int p_vitesse, int p_genre,
               int rythme_reproduction, int rtlcbdt): Organisme(univers_ID, false, index),
                                         m_age_max(age_max), m_faim_max(faim_max),
                                         genre(p_genre), m_attente_reproduction(rythme_reproduction),
                                         m_repas_tt_les_cb_de_tours(rtlcbdt), vitesse(p_vitesse)
{ }

Animal::Animal(const int univers_ID, const int age_max,
               const int faim_max, const int p_vitesse,
               int p_genre, int rythme_reproduction,
               int rtlcbdt): Organisme(univers_ID, false),
                             m_age_max(age_max), m_faim_max(faim_max),
                             genre(p_genre), m_attente_reproduction(rythme_reproduction),
                             m_repas_tt_les_cb_de_tours(rtlcbdt), vitesse(p_vitesse)
{ }

Animal::~Animal(void) {
    suppresionVecteurs();

    // Si l'animal se reproduisait, on annule pour la mère
    if(m_partenaire != nullptr) {
        m_partenaire->m_partenaire = nullptr;
        m_partenaire->m_reproduire = -1;
    }

    // Vérifie si l'ont doit déposé des sédiments
    if(m_deposer_sediment) {
        // Vérfie que il n'y a rien sur la case
        auto debut = Univers::m_index_libres_univers[m_univers_ID].first.begin();
        auto fin = Univers::m_index_libres_univers[m_univers_ID].first.end();
        auto it = std::find(debut, fin, m_index);
        if(it != fin) { // il n'y a rien, on dépose des sels minéraux sur la case
            new Sel(m_univers_ID, m_index);
        } // else -> on ne fais rien
    }
}

std::vector<int> Animal::casesAlentours(void) const noexcept {
    std::vector<int> vec;

    int longueur_univers = Univers::m_dimensions_univers[m_univers_ID].first,
        taille_max_univers = longueur_univers * Univers::m_dimensions_univers[m_univers_ID].second,
        i;

    // En haut à gauche
    i = m_index - longueur_univers - 1;
    if(i >= 0 && i % longueur_univers < longueur_univers - 1) {
        vec.push_back(i);
    }

    // En haut
    i = m_index - longueur_univers;
    if(i >= 0) {
        vec.push_back(i);
    }

    // En haut à droite
    i = m_index - longueur_univers + 1;
    if(i >= 0 && i % longueur_univers != 0) {
        vec.push_back(i);
    }

    // A gauche
    i = m_index - 1;
    if(i >= 0 && i % longueur_univers < longueur_univers - 1) {
        vec.push_back(i);
    }

    // A droite
    i = m_index + 1;
    if(i < taille_max_univers && i % longueur_univers != 0) {
        vec.push_back(i);
    }

    // En bas à gauche
    i = m_index + longueur_univers - 1;
    if(i < taille_max_univers && i % longueur_univers < longueur_univers - 1) {
        vec.push_back(i);
    }

    // En bas
    i = m_index + longueur_univers;
    if(i < taille_max_univers) {
        vec.push_back(i);
    }

    // En bas à droite
    i = m_index + longueur_univers + 1;
    if(i < taille_max_univers && i % longueur_univers != 0) {
        vec.push_back(i);
    }

    return vec;
}

std::vector<int> Animal::casesPossible(void) const noexcept {
    std::vector<int> vec = casesAlentours();

    // On retire ceux qui sont déjà occupés par un animal
    // C'est à dire pas dans la liste des index libre
    auto debut = Univers::m_index_libres_univers[m_univers_ID].second.begin();
    auto fin = Univers::m_index_libres_univers[m_univers_ID].second.end();
    auto it = vec.begin();
    while(it != vec.end()) {
        auto recherche = std::find(debut, fin, *it);
        if(recherche == fin) { // si non-libre on supprime
            vec.erase(it);
        } else { // sinon on va à la prochaine position possible
            ++it;
        }
    }

    return vec;
}

int Animal::choixGenre(void) const noexcept {
    std::random_device nombre_aleatoire;
    std::default_random_engine graine(nombre_aleatoire());
    std::uniform_int_distribution<int> aleatoire(0, 1);

    return aleatoire(graine);
}

void Animal::deplacement(int index, bool verification) noexcept {
    // Si l'animal n'est pas entrain de s'accoupler et s'il n'est pas déjà à destination
    if(m_partenaire == nullptr && index != m_index) {
        if(verification) {
            // On vérifie que la destination est possible
            std::vector<int> vec = casesAlentours();

            auto debut = vec.begin();
            auto fin = vec.end();
            auto it = std::find(debut, fin, index);
            if(it == fin) {
                std::cerr << "Impossible de déplacer l'animal vers cette position." << std::endl;
                return;
            }
        }

        // Ajoute l'index actuel au vecteurs des index libres
        Univers::m_index_libres_univers[m_univers_ID].second.push_back(m_index);

        std::cout << "'" << lettre(ID) << "' : " << coordoneeeEchequier() << " -> ";

        // Déplace l'animal
        m_index = index;

        std::cout << coordoneeeEchequier() << std::endl;

        // Retire la nouvel position du vecteur des index libre
        auto debut = Univers::m_index_libres_univers[m_univers_ID].second.begin();
        auto fin = Univers::m_index_libres_univers[m_univers_ID].second.end();
        auto it = std::find(debut, fin, m_index);
        if(it != fin) {
            Univers::m_index_libres_univers[m_univers_ID].second.erase(it);
        }
    }

}

void Animal::deplacement(void) noexcept { // déplacement aléatoire
    std::vector<int> cases_possible = casesPossible();
    // On rajoute l'index actuel car l'animal peut décidé de rester sur place
    cases_possible.push_back(m_index);

    std::random_device nombre_aleatoire;
    std::default_random_engine graine(nombre_aleatoire());
    std::uniform_int_distribution<int> aleatoire(0, cases_possible.size() - 1);

    deplacement(cases_possible[static_cast<uint64_t>(aleatoire(graine))], false);
}
