#include "../includes/univers.hpp"

Univers::Univers(const int longueur, const int largeur): m_longueur(longueur),
                                                         m_largeur(largeur),
                                                         m_taille_univers(longueur * largeur),
                                                         m_tour(0),
                                                         ID(m_total_ID + 1) {
    m_total_ID = ID; // + 1 aux ID

    // On ajoute tous les index possibles car pour l'instant car le plateau est vide
    for(int i = 0; i < m_taille_univers; ++i) {
        m_index_libres_univers[ID].first.push_back(i);
        m_index_libres_univers[ID].second.push_back(i);
    }

    // On mélange nos vecteur d'index non occupés
    melange(&m_index_libres_univers[ID].first);
    melange(&m_index_libres_univers[ID].second);

    // Remplie quelques valeurs du tableau avec de l'herbe
    while(m_index_libres_univers[ID].first.size() > static_cast<uint64_t>(m_taille_univers - m_taille_univers / 2)) {
                                          // cast static grâce à "-Wold-style-cast" et "-Wsign-conversion"
        new Herbe(ID);
    }

    // On stocke les dimensions pour chaque univers dans une map
    m_dimensions_univers[ID] = std::make_pair(largeur, longueur);
}

Univers::~Univers(void) {
    while(m_organismes_univers[ID].size()) { // Supprime les organismes présent dans l'univers
        delete m_organismes_univers[ID].back(); // l'organisme va se retirer tout seul du vecteur
    }
}

void Univers::melange(std::vector<int> * vecteur) noexcept {
    std::random_device nombre_aleatoire;
    std::default_random_engine graine(nombre_aleatoire());
    std::shuffle(vecteur->begin(), vecteur->end(), graine);
}

bool Univers::enVie(void) const noexcept {
    if(m_index_libres_univers[ID].second.size() == static_cast<uint64_t>(m_taille_univers)) { // si toutes les places animals sont libres
        return false; // aucun animal n'est en vie, univers mort
    }
    return true; // sinon l'univers est vivant
}

void Univers::nettoyageMorts(void) noexcept {
    auto it = m_organismes_univers[ID].begin();
    while(it != m_organismes_univers[ID].end()) { // parcours complet du vecteur
        if((*it)->mort) { // si l'organisme est mort
            delete *it; // on le supprime
        } else { // sinon on va au prochain organisme du vecteur
            ++it;
        }
    }
}
