#include "../includes/evenement.hpp"

void Evenement::creationSimulation(Univers ** univers, const int m, const int n, int nb_moutons, int nb_loups) {
    *univers = new Univers(m, n);

    while(nb_moutons > 0) { // Création des moutons dans l'univers
        new Mouton((*univers)->ID);
        nb_moutons--;
    }
    while(nb_loups > 0) { // Création des loups dans l'univers
        new Loup((*univers)->ID);
        nb_loups--;
    }
}

void Evenement::arreterSimulation(Univers * univers) {
    delete univers;
    univers = nullptr;
}

void Evenement::affichage(Univers * univers, const bool traits) noexcept {
    // Entête
    std::cout << "Affichage de l'univers n°" << univers->ID << " au tour n°" << univers->m_tour << "." << std::endl;

    // On génère le plateau pour l'affichage
    int * plateau = new int[univers->m_taille_univers]();
    for(auto it: univers->m_organismes_univers[univers->ID]) {
        // Vérification : un animal doit avoir la priorité sur
        // un autre organisme dans l'affichage
        if(plateau[it->position().first] == 0) { // pas de souci, aucun organisme à cette position
            plateau[it->position().first] = it->ID;
        } else { // souci : un organisme est déjà présent
                 // on sait déjà qu'un animal ne se superpose pas sur
                 // une autre case animal, donc c'est soit :
                 // - un animal sur une case non-animal
                 // - l'inverse, un non-animal sur une case animal
            if(dynamic_cast<Animal *>(it)) { // si c'est un animal
                plateau[it->position().first] = it->ID;
            } // sinon ne fait rien
        }
    }

    int largeur_affichage = univers->m_largeur * 4;

    // On affiche les coordonées type "échequier" seulement
    // si on a assez de lettre et que le nombre est inférieur à 100 (3 caractères)
    char echequier = 0;

    if(univers->m_longueur <= 26 && univers->m_largeur < 100) {
        std::cout << "   "; // espace du début
        for(int i = 0; i < univers->m_largeur; ++i) { // pour toute la largeur
            if(i < 9 && univers->m_largeur > 9) { // si on affiche + 9 nombres on ajoute
                                        // un 0 au 9 premiers
                std::cout << "0";
            } else {
                if (i < 9) { // sinon si on affiche que max 9 nombres on met un espace
                    std::cout << " ";
                }
            }
            std::cout << static_cast<int>(++echequier) << "  "; // cast en int
        }
        std::cout << std::endl;
    }
    echequier = 'A';

    for(int i = 0; i < largeur_affichage; ++i) {
        if(i == 0) { // coin supérieur gauche
            if(univers->m_longueur <= 26 && univers->m_largeur < 100) { // espace si il faut afficher l'echequier
                std::cout << "  ";
            }
            std::cout << "┌";
        } else { // haut
            std::cout << "─";
        }
    }
    std::cout << "┐" << std::endl; // coin supérieur droit

    // premier côté gauche
    if(univers->m_longueur <= 26 && univers->m_largeur < 100) { // espace pour l'alignement du tableau si nécessaire
        std::cout << echequier++ << " ";
    }
    std::cout << "│ ";

    for(int i = 0; i < univers->m_taille_univers; i += univers->m_largeur) {
        for(int j = 0; j < univers->m_largeur; ++j) {
            std::cout << Organisme::lettre(plateau[i + j]);
            if(j == univers->m_largeur - 1) {
                std::cout << " │ "; // côté droit
            } else { // espace dans le tableau
                if(traits) {
                    std::cout << " ┊ ";
                } else {

                    std::cout << "   ";
                }
            }
        }
        if(i != univers->m_taille_univers - univers->m_largeur) {
            std::cout << std::endl;
            if(univers->m_longueur <= 26 && univers->m_largeur < 100) {
                std::cout << "  ";
            }

            // ligne vide
            std::cout << "│";
            if(traits) {
                for(int j = 0; j < univers->m_largeur - 1; ++j) {
                    std::cout << "---+";
                }
                std::cout << "---";
            } else {
                std::cout << std::string(static_cast<uint64_t>(largeur_affichage - 1), ' '); // saut de ligne et remplissage
                                                                                            // de la ligne de vide pour y mettre
                                                                                            // le caractère fermant le tableau
            }
            std::cout << "│" << std::endl;

            // saut de ligne et suite du côté gauche
            if(univers->m_longueur <= 26 && univers->m_largeur < 100) {
                std::cout << echequier++ << " ";
            }
            std::cout << "│ ";
        }
    }

    std::cout << std::endl;
    for(int i = 0; i < largeur_affichage; ++i) {
        if(i == 0) {
            if(univers->m_longueur <= 26 && univers->m_largeur < 100) { // espace si il faut afficher l'echequier
                std::cout << "  ";
            }
            std::cout << "└"; // coin inférieur gauche
        } else {
            std::cout << "─"; // bas
        }
    }
    std::cout << "┘"  << std::endl; // coin inférieur droit

    delete[] plateau;

    std::cout << std::string(50, '-') << std::endl;
}

void Evenement::lancerSimulation(Univers * univers) {
    std::cout << "Lancement de la simulation pour l'univers n°" << univers->ID <<  "..." << std::endl;

    // A chaque tour de l'univers, on trie les organismes en fonction de leur vitesse
    // pour déterminer l'ordre des actions, puis chaque organisme fait une action
    // A la fin de chaque tour, on incrémente de 1 le nombre de tours
    while(univers->enVie()) {
        // Incrémente le nombre de tours
        ++univers->m_tour;

        // On trie les organismes en fonction de leur vitesse
        std::sort(univers->m_organismes_univers[univers->ID].begin(), univers->m_organismes_univers[univers->ID].end(), comp_organisme());

        // Fais ce qu'il a à faire pendant son tour
        // pour tout les organismes vivant de notre univers
        for(auto it: univers->m_organismes_univers[univers->ID]) {
            if(!it->mort) {
                it->action();
            }
        }

        // Supprime les organismes mort du vecteur `Univers::m_organismes_univers`
        univers->nettoyageMorts();

        std::cout << std::endl;
        affichage(univers);
    }

    std::cout << "Fin de la simulation pour l'univers n°" << univers->ID <<  " !" << std::endl;
}
