#include "../includes/organisme.hpp"
#include "../includes/univers.hpp" // on import ici et pas dans le .hpp car l'univers importe lui meme
                                   // organisme.hpp, donc si on importe univers.hpp dans organisme.hpp
                                   // alors univers.hpp ne pourra pas s'initalisé correctement

Organisme::Organisme(const int univers_ID, const bool superposable, const int index): m_univers_ID(univers_ID), m_index(index),
                                                                                      ID(m_total_ID + 1), m_superposable(superposable) {
    // S'il n'y a plus d'index de libres
    if(superposable) {
        if(Univers::m_index_libres_univers[m_univers_ID].first.size() == 0) {
            throw std::domain_error("Trop d'organismes \"superposable\" dans l'univers.");
        }
    } else {
        if(Univers::m_index_libres_univers[m_univers_ID].second.size() == 0) {
            throw std::domain_error("Trop d'organismes \"non-superposable\" dans l'univers.");
        }
    }
    m_total_ID = ID; // + 1 aux ID

    // Ajoute l'organisme à la liste des organismes de l'univers
    Univers::m_organismes_univers[m_univers_ID].push_back(this);

    // Assigne un index à l'organisme dans l'univers
    if(superposable) {
        if(index == -1) { // aléatoire si non-renseigné
            m_index = Univers::m_index_libres_univers[m_univers_ID].first.back();
        }

        // Retire l'index de la liste des index libres (superposable)
        auto debut = Univers::m_index_libres_univers[m_univers_ID].first.begin();
        auto fin = Univers::m_index_libres_univers[m_univers_ID].first.end();
        auto it = std::find(debut, fin, m_index);
        if(it != fin) {
            Univers::m_index_libres_univers[m_univers_ID].first.erase(it);
        }
    } else {
        if(index == -1) { // aléatoire si non-renseigné
            m_index = Univers::m_index_libres_univers[m_univers_ID].second.back();
        }

        // Retire l'index de la liste des index libres (non-superposable)
        auto debut = Univers::m_index_libres_univers[m_univers_ID].second.begin();
        auto fin = Univers::m_index_libres_univers[m_univers_ID].second.end();
        auto it = std::find(debut, fin, m_index);
        if(it != fin) {
            Univers::m_index_libres_univers[m_univers_ID].second.erase(it);
        }
    }
}

Organisme::~Organisme(void) { }

char Organisme::lettre(const int id) noexcept {
    if(m_correspondance[id]) {
        return m_correspondance[id];
    }

    return ' ';
}

std::pair<int, std::pair<int, int>> Organisme::position(void) const noexcept {
    return std::make_pair(m_index, std::make_pair(m_index / Univers::m_dimensions_univers[m_univers_ID].first, m_index % Univers::m_dimensions_univers[m_univers_ID].first));
}

std::string Organisme::coordoneeeEchequier(void) const noexcept {
    int x = position().second.second + 1;
    char y = 'A' + position().second.first;
    return y + std::to_string(x);
}

void Organisme::mortOrganisme(void) noexcept {
    // On remet notre index dans le vecteur des index vide
    // puis on remélange notre vecteur
    if(m_superposable) { // vecteur différent en fonction du type
        Univers::m_index_libres_univers[m_univers_ID].first.push_back(m_index);
        Univers::melange(&Univers::m_index_libres_univers[m_univers_ID].first);
    } else {
        Univers::m_index_libres_univers[m_univers_ID].second.push_back(m_index);
        Univers::melange(&Univers::m_index_libres_univers[m_univers_ID].second);
    }

    // On déclare mort l'organisme
    mort = true;

    std::cout << "Mort de '" << lettre(ID) << "' (" << coordoneeeEchequier() << ")" << std::endl;
}

void Organisme::suppresionVecteurs(void) noexcept {
    // On se supprime du vecteur
    auto debut = Univers::m_organismes_univers[m_univers_ID].begin();
    auto fin = Univers::m_organismes_univers[m_univers_ID].end();
    auto it = std::find(debut, fin, this);
    if(it != fin) {
        Univers::m_organismes_univers[m_univers_ID].erase(it);
    }
}
