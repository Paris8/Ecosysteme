#include "../includes/mouton.hpp"

Mouton::Mouton(const int univers_ID): Animal(univers_ID, 50, 5, Mouton::generationVitesse(), Mouton::choixGenre(), 1, 1) {
    m_correspondance[ID] = _m_lettre;
}

Mouton::Mouton(const int univers_ID, const int index): Animal(univers_ID, index, 50, 5, Mouton::generationVitesse(), Mouton::choixGenre(), 1, 1) {
    m_correspondance[ID] = _m_lettre;
}

int Mouton::generationVitesse(void) const noexcept {
    std::random_device nombre_aleatoire;
    std::default_random_engine graine(nombre_aleatoire());
    std::uniform_int_distribution<int> aleatoire(0, 15);

    return aleatoire(graine);
}

bool Mouton::carnivore(void) const noexcept {
    return false;
}

void Mouton::action(void) {
    if(m_age > m_age_max || m_faim > m_faim_max) { // meurt si trop vieux ou trop faim
        mortOrganisme();
        return;
    } else {
        ++m_age; // augmente l'âge
        ++m_faim; // augmente la faim
    }

    // Se déplace aléatoirement d'une case
    deplacement();

    // Mange si besoin
    if(m_faim >= m_repas_tt_les_cb_de_tours && m_partenaire == nullptr) {
        for(auto organisme: Univers::m_organismes_univers[m_univers_ID]) { // regarde tout les organismes
            if(!organisme->mort) { // si l'organisme n'est pas mort
                if(organisme->position().first == m_index) { // si 1 organisme sur ma position
                    if(dynamic_cast<Herbe *>(organisme)) { // si c'est de l'herbe
                        organisme->mortOrganisme();
                        std::cout << "'" << lettre(ID) << "' (" << coordoneeeEchequier() << ") mange '" << lettre(organisme->ID) << "' (" << organisme->coordoneeeEchequier() << ")" << std::endl;
                        m_faim = 0;
                        break;
                    }
                }
            }
        }
    }

    // S'accouple si besoin
    procreation<Mouton>();
}
