#include "../includes/herbe.hpp"

Herbe::Herbe(const int univers_ID, const int index): Organisme(univers_ID, true, index) {
    m_correspondance[ID] = _m_lettre;
}

Herbe::Herbe(const int univers_ID): Organisme(univers_ID) {
    m_correspondance[ID] = _m_lettre;
}

Herbe::~Herbe(void) {
    suppresionVecteurs();
}

void Herbe::action(void) { /* ne fais rien */ }
