#include <cstring>

#include "../includes/evenement.hpp"

void erreurArguments(char const * nom_programme) {
    std::cerr << "Arguments mal renseignés." << std::endl;
    std::cout << "Usage : " << nom_programme << " m n nb_moutons nb_loups" << std::endl;
    /* std::cout << "Usage : " << nom_programme << " -b/--benchmark m n [e]" << std::endl; */
    exit(1);
}

// m x n      = taille de l'univers
// nb_moutons = nombre de moutons
// nb_loups   = nombre de loups
int main(int argc, char const * argv[]) {
    if(argc != 1 /* && argc != 4 */ && argc != 5) {
        erreurArguments(argv[0]);
    }
    int m, n, nb_moutons, nb_loups;
    if(argc == 1) { // valeurs par défaut
        m = 5;
        n = 6;
        nb_moutons = 9;
        nb_loups   = 4;
    } else {
        if(strcmp(argv[1], "-b") == 0 || strcmp(argv[1], "--benchmark") == 0) {
            std::cout << "Benchmark pas encore implanté." << std::endl;
            exit(1);
        } else {
            if(argc == 5) { // renseigné par l'utilisateur
                m = std::stoi(argv[1]);
                n = std::stoi(argv[2]);
                nb_moutons = std::stoi(argv[3]);
                nb_loups   = std::stoi(argv[4]);
            } else { // argc = 4 et pourtant pas de "-b"
                erreurArguments(argv[0]);
            }
        }
    }

    Univers * univers = nullptr;
    Evenement::creationSimulation(&univers, m, n, nb_moutons, nb_loups);

    Evenement::affichage(univers);

    Evenement::lancerSimulation(univers);

    Evenement::affichage(univers);

    Evenement::arreterSimulation(univers);

    return 0;
}
