#include "../includes/loup.hpp"

Loup::Loup(const int univers_ID): Animal(univers_ID, 60, 10, Loup::generationVitesse(), Loup::choixGenre(), 3, 4) {
    m_correspondance[ID] = _m_lettre;
}

Loup::Loup(const int univers_ID, const int index): Animal(univers_ID, index, 60, 10, Loup::generationVitesse(), Loup::choixGenre(), 3, 4) {
    m_correspondance[ID] = _m_lettre;
}

int Loup::generationVitesse(void) const noexcept {
    std::random_device nombre_aleatoire;
    std::default_random_engine graine(nombre_aleatoire());
    std::uniform_int_distribution<int> aleatoire(5, 20);

    return aleatoire(graine);
}

bool Loup::carnivore(void) const noexcept {
    return true;
}

void Loup::action(void) {
    if(m_age > m_age_max || m_faim > m_faim_max) { // meurt si trop vieux ou trop faim
        mortOrganisme();
        return;
    } else {
        ++m_age; // augmente l'âge
        ++m_faim; // augmente la faim
    }

    // Le déplacement est différent du mouton, le loup
    // va regarder toutes les cases autour de lui, s'il a faim il va pouvoir
    // se déplacer vers une case où un mouton se trouve et le manger, s'il n'a
    // pas faim alors il va se déplacer comme un mouton
    bool deplacement_effectue = false;
    if(m_faim >= m_repas_tt_les_cb_de_tours && m_partenaire == nullptr) { // mange si besoin
        std::vector<int> cases_aux_alentours = casesAlentours();
        for(auto organisme: Univers::m_organismes_univers[m_univers_ID]) { // regarde tout les organismes
            if(deplacement_effectue) {
                break;
            }
            if(!organisme->mort) { // si l'organisme n'est pas mort
                if(auto proie = dynamic_cast<Mouton *>(organisme)) { // si c'est un mouton
                    for(auto it: cases_aux_alentours) { // regarde toutes les cases aux alentours
                        if(proie->position().first == it) { // si 1 mouton sur la position
                            proie->m_deposer_sediment = false;
                            proie->mortOrganisme();
                            m_faim = 0;
                            std::cout << "'" << lettre(ID) << "' (" << coordoneeeEchequier() << ") mange '" << lettre(proie->ID) << "' (" << proie->coordoneeeEchequier() << ")" << std::endl;
                            deplacement(proie->position().first);
                            deplacement_effectue = true;
                            break;
                        }
                    }
                }
            }
        }
    }

    if(!deplacement_effectue) {
        deplacement();
    }

    // S'accouple si besoin
    procreation<Loup>();
}
