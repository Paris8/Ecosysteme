---
controlsLayout: edges
---
# Écosystème
> Anri KENNEL $\cdot$ L2-X
---
## Relation des classes
![schema](img/schema.svg)
---
## `main.cpp`
- Gère les arguments aux programmes
- Lance, affiche et arrête la simulation (`Evenement`)

---
## `Evenement`
- Créer la simulation (`Univers`)
    - Ajoute les animaux
- Supprime l'univers
- Affiche un univers à un instant $t$
- Fait vivre l'univers
    - Appel chaque organisme de l'univers
        - Ordre: du + rapide au - rapide

---
## `Univers`
- ID unique
- Stocke
    - les dimensions de l'univers (`map`)
        - ID univers correspond aux dimensions
    - les organismes de l'univers (`map` et `vector`)
        - ID univers corresponds aux vecteur d'organismes
---
## `Univers`
- Stocke
    - les cases vides de l'univers (`map`, `pair` et `vector`)
        - ID univers correspond à une pair de vecteur
            - l'un pour les animaux (mouton, loup)
            - l'autre pour les non-animaux (sel, herbe)
- Vérifie si l'univers est en vie (`enVie`)
- Supprime les organismes mort (`nettoyageMorts`)

---
## `Organisme`
- ID unique
- Stocke
    - ID correspond à une lettre pour l'affichage (`map`)
    - index dans l'univers
    - ID univers
- Peut se supprimer d'un univers (`suppressionVecteurs`) 

---
## `Organisme`
- `lettre`, exemple, un mouton à la lettre "M"
- Peut faire une action
- Peut mourir (`mortOrganisme`)
- Peut renvoyer la position sous forme
    -  x, y
    - index
    - echequier (A4)

---
## `Animal`
Dépend de la classe `Organisme`
- Stocke
    - age et age max que l'animal peut atteindre
    - faim et faim max que l'animal peut supporter
    - s'il l'animal se reproduit, avec qui ?, cb de temps avant de pouvoir se reproduire ?
    - genre (masculin, féminin)
    - vitesse
    - doit-il poser des sédiment à la mort?

---
## `Animal`
- Peut se déplacer
- Peut s'accoupler
- Regarde les cases aux alentours où il peut aller ou non
- S'il est carnivore

---
## `Sel`,  `Herbe`,  `Mouton`,  `Loup`
`Sel` et `Herbe` dépendent de la classe `Organisme`
`Mouton` et `Loup` dépendent de la classe `Animal`

Seul le comportement des méthodes virtuelles sont définies dans ses classes.