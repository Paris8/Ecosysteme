CXX      = g++
CXXFLAGS = --std=c++17
RM       = rm

SOURCES  = $(wildcard src/*.cpp)
OBJETS   = $(patsubst %.cpp,%.cpp.o,$(notdir $(SOURCES)))

EXE      = ecosysteme

%.cpp.o: src/%.cpp
	$(CXX) -c -o $@ $< $(CXXFLAGS) $(DEVFLAGS)

main: CXXFLAGS += -O3
main: compilation

dev: CXXFLAGS += -Wall -Wextra -Wshadow -Wnon-virtual-dtor -pedantic -g -Wold-style-cast -Wsign-conversion
dev: compilation

compilation: $(OBJETS)
	$(CXX) -o $(EXE) $(OBJETS)

all:
	main

clean:
	$(RM) $(OBJETS) $(EXE)
